## Dependable

Package for managing dependency sorting. Useful for sorting arrays of lists, scripts, etc...

## API Documentation

API documentation can be found in the api directory.

### License

This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
