<?php

namespace Schenley\Dependable;

use Illuminate\Support\Collection;
use MJS\TopSort\Implementations\StringSort;
use RuntimeException;

/**
 * Part of the Dependable package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Dependable
 * @version    1.0.0
 * @author     Schenley Learning
 * @license    MIT License
 * @copyright  (c) 2015, Schenley Learning, LLC
 */

class Sorter extends Collection {

	protected $items = [];


	/**
	 * Creates a new instance of the Sorter class
	 *
	 * @param array $items
	 */
	function __construct(array $items = [])
	{
		$this->sorter = $this->setSorter();

		if ( ! empty($items))
		{
			foreach ($items as $item)
			{
				$this->add($item);
			}
		}
	}

	/**
	 * Adds a new Item to the Sorter & Collection
	 *
	 * @param $item
	 * @param array $dependencies
	 */
	public function add($item, $dependencies = [])
	{
		if ( ! $item instanceof Dependable)
		{
			throw new RuntimeException("Items must extend the Dependable contract to be sorted.");
		}

		$this->items[$item->getSlug()] = $item;
	}

	/**
	 * Sorts the Collection
	 *
	 * @return array|\string[]
	 */
	public function order()
	{
		foreach ($this->items as $item)
		{
			$this->sorter->add($item->getSlug(), $item->getDependencies());
		}

		$this->resortItems($this->sorter->sort());

		return $this;
	}

	/**
	 * Resorts our items based off the sorted array
	 *
	 * @param $sorted
	 */
	public function resortItems($sorted)
	{
		$ordered = [];

		foreach ($sorted as $slug)
		{
			$ordered[$slug] = $this->get($slug);
		}

		$this->items = $ordered;

		unset($ordered); unset($sorted);
	}

	/**
	 * Sets our Sorter
	 *
	 * @return StringSort
	 */
	public function setSorter()
	{
		return new StringSort;
	}
}
